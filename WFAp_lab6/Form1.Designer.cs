﻿namespace WFAp_lab6
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAdd = new System.Windows.Forms.Button();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.buttonShow = new System.Windows.Forms.Button();
            this.textBoxOpakowanie = new System.Windows.Forms.TextBox();
            this.textBoxKategoria = new System.Windows.Forms.TextBox();
            this.labelKategoria = new System.Windows.Forms.Label();
            this.labelOpakowanie = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(156, 79);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(85, 23);
            this.buttonAdd.TabIndex = 0;
            this.buttonAdd.Text = "buttonAdd";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Location = new System.Drawing.Point(70, 182);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(122, 21);
            this.comboBox.TabIndex = 1;
            this.comboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectedIndexChanged);
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(92, 209);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(75, 23);
            this.buttonShow.TabIndex = 2;
            this.buttonShow.Text = "buttonShow";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // textBoxOpakowanie
            // 
            this.textBoxOpakowanie.Location = new System.Drawing.Point(140, 39);
            this.textBoxOpakowanie.Name = "textBoxOpakowanie";
            this.textBoxOpakowanie.Size = new System.Drawing.Size(121, 20);
            this.textBoxOpakowanie.TabIndex = 5;
            // 
            // textBoxKategoria
            // 
            this.textBoxKategoria.Location = new System.Drawing.Point(140, 9);
            this.textBoxKategoria.Name = "textBoxKategoria";
            this.textBoxKategoria.Size = new System.Drawing.Size(121, 20);
            this.textBoxKategoria.TabIndex = 6;
            // 
            // labelKategoria
            // 
            this.labelKategoria.AutoSize = true;
            this.labelKategoria.Location = new System.Drawing.Point(39, 13);
            this.labelKategoria.Name = "labelKategoria";
            this.labelKategoria.Size = new System.Drawing.Size(35, 13);
            this.labelKategoria.TabIndex = 7;
            this.labelKategoria.Text = "label1";
            // 
            // labelOpakowanie
            // 
            this.labelOpakowanie.AutoSize = true;
            this.labelOpakowanie.Location = new System.Drawing.Point(42, 39);
            this.labelOpakowanie.Name = "labelOpakowanie";
            this.labelOpakowanie.Size = new System.Drawing.Size(35, 13);
            this.labelOpakowanie.TabIndex = 8;
            this.labelOpakowanie.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.labelOpakowanie);
            this.Controls.Add(this.labelKategoria);
            this.Controls.Add(this.textBoxKategoria);
            this.Controls.Add(this.textBoxOpakowanie);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.comboBox);
            this.Controls.Add(this.buttonAdd);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.TextBox textBoxOpakowanie;
        private System.Windows.Forms.TextBox textBoxKategoria;
        private System.Windows.Forms.Label labelKategoria;
        private System.Windows.Forms.Label labelOpakowanie;
    }
}

