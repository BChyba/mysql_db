﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using WFAp_lab6.SqlConnect;

namespace WFAp_lab6.Manual
{
    class ComboBoxManual
    {

        public List<string> loadComboBox_from_table(string fieldOne, string fieldTwo, string tableName, ComboBox listComboBox)
        {
            //listbox
            listComboBox.Items.Clear();
            List<string> content_loadComboBox_from_table = new List<string>();

            //Connection
            ConnectMySql connectionMySql = new ConnectMySql();
            MySqlConnection connection = connectionMySql.connect();
            MySqlDataReader summary;

            // Create New Item
            string tabela = "SELECT " + fieldOne + ", " + fieldTwo + " FROM " + tableName + ";";
            MySqlCommand question = new MySqlCommand(tabela, connection);
            summary = question.ExecuteReader();
            while (summary.Read())
            {
                content_loadComboBox_from_table.Add(summary.GetString(1));
                listComboBox.Items.Add(summary.GetString(1));
            }
            connectionMySql.Close(connection);

            return content_loadComboBox_from_table;
        }
    }
}
