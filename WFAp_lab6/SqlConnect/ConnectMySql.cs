﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WFAp_lab6.SqlConnect
{

    class ConnectMySql
    {
        public MySqlConnection connection;
        public static string strConnect = "server=localhost;user=root;database=mydb;DefaultTableCacheAge=30 ;charset=utf8;";

        // COnnect
        public MySqlConnection connect()
        {

            connection = new MySqlConnection(strConnect);
            try
            {
                connection.Open();
                MessageBox.Show("Connection is Strong!");

            }
            catch (Exception e)
            {
                MessageBox.Show("There is no connection " + e.Message);
            }
            return connection;
        }

        //Disconnect
        public void Close(MySqlConnection connection)
        {
            connection.Close();
        }

    }
}
