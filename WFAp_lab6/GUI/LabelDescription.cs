﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFAp_lab6.GUI;

namespace WFAp_lab6.GUI
{
    class LabelDescription
    {

        //etykiety
        #region

        public static string LabelOpakowanie = "Opakowanie";
        public static string LabelKategoria = "Kategoria";

        #endregion


        // guziki
        #region

        public static string ButtonShow = "Pokaż";
        public static string ButtonAdd = "Dodaj";

        #endregion

    }
}
