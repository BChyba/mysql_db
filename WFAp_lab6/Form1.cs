﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using WFAp_lab6.Manual;
using WFAp_lab6.SqlConnect;
using WFAp_lab6.GUI;

namespace WFAp_lab6
{
    public partial class Form1 : Form
    {
        ComboBoxManual comboBoxManual = new ComboBoxManual();
        List<string> IDfromTable = new List<string>();

        ButtonAddManual buttonManualAdd = new ButtonAddManual();

        public Form1()
        {
            InitializeComponent();
        }



        private void buttonAdd_Click(object sender, EventArgs e)
        {

            buttonManualAdd.buttonAddMaual(textBoxOpakowanie.Text, textBoxKategoria.Text);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBoxManual.loadComboBox_from_table("Kategoria", "Opakowanie", "napoje", comboBox);

            this.labelKategoria.Text = LabelDescription.LabelOpakowanie;
            this.labelOpakowanie.Text = LabelDescription.LabelKategoria;

            this.buttonAdd.Text = LabelDescription.ButtonAdd;
            this.buttonShow.Text = LabelDescription.ButtonShow;
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            // MessageBox.Show(IDfromTable[comboBox.SelectedIndex]);
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           // IDfromTable = comboBoxManual.loadComboBox_from_table("Opakowanie", "Kategoria", "napoje", comboBox);
        }

    }
}
